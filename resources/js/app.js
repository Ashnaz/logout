import Vue from 'vue';
import VueSwal from 'vue-swal'
import vuetify from './vuetify'

require('./bootstrap');

window.Vue = require('vue');
Vue.use(VueSwal);

Vue.component('signout-component',require('./components/signout/SignOutComponent.vue').default)
Vue.component('paymentsense-header-component',require('./components/campaign/PaymentSenseHeaderComponent.vue').default)
Vue.component('paymentsense-elements-component',require('./components/campaign/PaymentSenseElementsComponent.vue').default)
Vue.component('paymentsense-contact-component',require('./components/campaign/PaymentSenseContactComponent.vue').default)
Vue.component('popup-component',require('./components/campaign/PopupComponent.vue').default)


const app = new Vue({
    vuetify,
    el: '#app',
});
