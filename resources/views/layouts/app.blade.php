<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    {{-- <script src="{{asset('js/jquery-ui.js') }}" defer></script> --}}
    
       


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Comfortaa" />

    <!-- Styles -->
    
    {{-- <link href="{{ asset('css/style.min.css') }}" rel="stylesheet"> --}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {{-- <link href="{{ asset('css/sementic-ui.css') }}" rel="stylesheet">
    <link href="{{asset('css/jquerysmooth.css')}}" rel="stylesheet"/> --}}
    
    <link rel="stylesheet" href="{{ asset('icons/css/material-design-iconic-font.min.css') }}" />
    
    
    
    
</head>

<body style=''>
       
       <div id='app'>
            @yield('content')
        
       </div>
    
</body>
</html>

