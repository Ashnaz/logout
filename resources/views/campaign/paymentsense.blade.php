@extends('layouts.app')
@section('pageTitle', 'Payment Sense')

@section('content')
  <div>
    {{-- <div id="root"> --}}
        {{-- @if((new \Jenssegers\Agent\Agent())->isPhone())
            <mobile-navbar-component></mobile-navbar-component>
        @else
            <new-navbar-component></new-navbar-component>
        @endif --}}
        <paymentsense-header-component></paymentsense-header-component>
        <paymentsense-elements-component></paymentsense-elements-component>
        <paymentsense-contact-component></paymentsense-contact-component>
            {{-- <popup-component></popup-component> --}}
        {{-- @if((new \Jenssegers\Agent\Agent())->isPhone())
            <sticky-footer-component></sticky-footer-component>
            <sticky-call-component></sticky-call-component>
        @endif --}}
    </div>

@endsection

@section('style')
    <style>
        .position-absolute {
            position: fixed !important;
        }

        .contactcomponent {
            padding-top: 0px;
        }
        @media only screen and (max-width: 768px) {
            .contactcomponent {
                padding-top: 10px;
            }
        }
    </style>
@endsection
