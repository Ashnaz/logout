<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/home', 'HomeController@index');



Route::get('/signout','SignOutController@index');



// Route::get('/getusers','SignOutController@getUsers');
// Route::get('/getusers/{id}','SignOutController@getSingleUser');
// Route::get('/getUserswithLevels','SignOutController@getUserswithLevels');
// Route::post('/posted','SignOutController@posted'); 

// Route::get('/getuserLogs/{id}','SignOutController@getuserLogs');


// Route::get('/getLevel','UserLevelController@userslevels');
// Route::get('/getLevelall','UserLevelController@userslevels');
// Route::get('/getuserlevelLogs/{id}','SignOutController@getuserlevelLogs');