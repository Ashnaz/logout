<?php


use Illuminate\Support\Facades\Route;

use Illuminate\Routing\Router;


Route::get('getusers','UserController@getUsers');
//Route::post('getusers','API\SignOutController@getUsers')->status();

//getting id from post API  and returning logs
Route::post('getuserlogs','SignOutController@getuserLogs');

//this will check return 3 tables data and return ,with corresponding data we should use 
Route::post('getuserlevelLogs','SignOutController@getuserlevelLogs');


//this will create new level , data should be forwarded to as an object and ->LevelName
Route::post('createlevels','UserLevelController@createlevel');

//this will create new user
Route::post('createuser','UserController@createUser');



//this will create new product
Route::post('createproduct','ProductController@createProduct');
Route::post('getproduct','ProductController@getProduct');
Route::post('getallproducts','ProductController@getAllProducts');






