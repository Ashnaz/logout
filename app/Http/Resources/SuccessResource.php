<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SuccessResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function with($request){
        return[
            "status"=>"success",
            "code"=>"red"
        ];
    }

    public function toArray($request)
    {
        return parent::toArray($request);
    }

    
}
