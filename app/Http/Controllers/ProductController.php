<?php

namespace App\Http\Controllers;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\GetProductRequest;
use Illuminate\Http\Request;
use App\Product;
use App\Department;
use App\testing;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;

class ProductController extends Controller
{
    public function createProduct(ProductRequest $request){
        try{
            Product::create($request->validated());
            return response()->json([
                "status"=>"200",
                "message"=>"success"
            ]);
        }
        catch(Throwable $err){
            return response()->json([
                "status"=>"404",
                "message"=>"failed"
            ]);
        }
    }

    public function getProduct(getProductRequest $request){
        $pro=Product::where("Barcode",$request->input('Barcode'))
         ->join('department','department.Depid','=','products.Depid')
         ->select('Proname','Sellingprice','Barcode','department.Depid','Depname')->get();
        return new ProductCollection($pro);
    }

    public function getAllProducts(){
       return new ProductCollection(Product::all());
    }


    public function store(GetProductRequest $request){
       return 'ppost';
    }

    public function get(){
    }

    public function index(GetProductRequest $request){
       return new ProductCollection(Product::all());
        
    }
    

    public function destroy(){
        return 'get is destroy';
    }

    


}
