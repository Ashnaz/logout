<?php


namespace App\Http\Controllers;


use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\DB;
use App\Users;
use App\UserLevel;

class UserController extends Controller
{
    public function index(){
        return view('signout.index');
    }

    public function getUsers(){
        $rs=Users::all();
        return response()->json([
            "status"=>"200",
            "message"=>"data received",
            "data"=>$rs
        ]);

    }

    public function getSingleUser($id){
        return $response=Users::where('id',$id)->get();
    }

    public function getUserswithLevels(){
        return $respone=Users::join('userlevel','userlevel.Id','=','userlevel_Id')
            ->select('Useraname','LevelName')->get();
    }

    public function getsingleuserLogs(){
        return $response=Users::where('userid',$id)->join('userlevel','levelId','=','userlevel_Id')
        ->select('Useraname','LevelName')->get();
    }


    //this will return userlogs with corresponding user id 
    public function getuserLogs(Request $request){
        // return $respone=Users::where('userid',$id)->join('userlog','loggedid','=','userid')
        // ->select('Useraname','userlog.*')->get();
        if ($request->isMethod('post')) {
            $user=new Users;
            $user=$request->all();
                $user->create();
                return 'success!';
            // return  json_encode($user);
        }else{
            return json_encode('false');
        }
        // return json_encode($request->path());

    }
    
    //this will check return 3 tables data and return ,with corresponding data
    public function getuserlevelLogs(Request $request){
        return $response=Users::where('userid',$request->input("id"))
        ->join('userlog','loggedid','=','userid')
        ->join('userlevel','levelId','=','userlevel_Id')
        ->select('Useraname','userlog.*','LevelName')->get();
    }

    
    
    //this create a new user 
    public function createUser(UserRequest $request){
            $user=new Users();
            $data=$request->validated();
            try{
              $user::create($data);
            return response()->json([
                "status"=>"200",
                "message"=>"successfully created a new user"
            ]);
            }
            catch(Throwable $err){
                return response()->json([
                "status"=> "502",
                "message"=>"failed"
            ]);
            }
    }



    
}
