<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserLevel;


class UserLevelController extends Controller
{
    public function userslevels(){
        
        return $response=UserLevel::all();
    }

    //this will create new level , data should be forwarded to as an object and ->LevelName
    public function createlevel(Request $request){
         $level=new UserLevel();
         $level->LevelName=$request->input('LevelName');
         if($level->LevelName!=null && $level->LevelName!=''){
            try{
                $level->save();
                return 'true';
             }
             catch(Throwable $e)
             {
                return 'faild';
             }
         }
         else{
             return 'please check ur inputs';
         }
    
    }

    
}
