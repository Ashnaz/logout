<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;



class UserLevel extends Model
{
    protected $table='userlevel';
    public $timestamps = false;
}
