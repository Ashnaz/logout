<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table="products";
     protected $fillable = ['Proname', 'Sellingprice', 'Depid','Barcode','Proid'];
    public $timestamps = false;
}
